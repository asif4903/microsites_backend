import { Request, Response, NextFunction } from 'express';
import { JWTUtils } from '../utils/jwt_utils';
import { User } from '../services/users';
export class AuthMiddleware {
    public static async isAuthenticated(req: Request, res: Response, next: NextFunction): Promise<any> {
        const user_service = new User();
        // Get the token from the request header
        const token = req.header('Authorization')?.replace('Bearer ', '');

        if (!token) {
            return res.status(401).json({ error: 'Unauthorized: Missing token' });
        }

        try {
            // Verify the token
            const decoded = JWTUtils.verifyAuthToken(token);
            console.log("decoded: " + JSON.stringify(decoded));
            const user = await user_service.getUserById(decoded.user_id as string);
            console.log("user: " + JSON.stringify(user));
            if (!user) {
                throw new Error("Unauthorized: Invalid token");
            }
            if (user.access_token !== token) {
                throw new Error("Unauthorized: Invalid token");
            }
            // Attach user data to the request for further use
            req.user = user;
            next();
        } catch (error) {
            console.error("here error: ", error);
            res.status(401).json({ error: 'Unauthorized: Invalid token' });
        }

    }
}