export class Utils {
  private static env_keys = ["PORT", "DB_HOST", "DB_PORT", "JWT_SECRET"];
  public static validateEnvironmentVariables() {
    console.info("checking environment variables required:------")
    for (const key of Utils.env_keys) {
      if (!process.env[key]) {
        console.error(`process.env.${key} environment variable is not provided. Exiting....`);
        process.exit(1);
      }
    }
    console.info("All environments variables are available:------")
  }
}
