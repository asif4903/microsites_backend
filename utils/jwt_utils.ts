import jwt, { JwtPayload } from 'jsonwebtoken';

export class JWTUtils {
    private static JWT_SECRET: string = process.env.JWT_SECRET as string;

    public static generateAuthToken(user_id: string): string {
        const token = jwt.sign({ user_id: user_id }, JWTUtils.JWT_SECRET);
        return token;
    }
    public static verifyAuthToken(token: string): JwtPayload {
        const decoded: JwtPayload = jwt.verify(token, JWTUtils.JWT_SECRET) as JwtPayload;
        return decoded;
    }
}