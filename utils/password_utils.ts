import bcrypt from 'bcrypt';

export class PasswordUtils {
    private static salt_rounds: number = 10;
    public static async hashPassword(password: string): Promise<string> {
        const hashed_password = await bcrypt.hash(password, PasswordUtils.salt_rounds);
        return hashed_password;
    }

    public static async comparePassword(password: string, hashed_assword: string): Promise<boolean> {
        const is_match = await bcrypt.compare(password, hashed_assword);
        return is_match;
    };

}