import mongoose from "mongoose";
import { IUser, UserDocument, UserSchema } from "../models/users";

export class User {
    private user_model = global.microsites_write_connection?.model("users", UserSchema);

    public async createUser(user_data: IUser): Promise<UserDocument> {
        const new_user = new this.user_model(user_data);
        return await new_user.save();
    }

    public async getUserById(user_id: string, proj: any = {}): Promise<UserDocument> {
        return await this.user_model.findOne({ _id: new mongoose.Types.ObjectId(user_id) }, proj).lean().exec() as UserDocument;
    }

    public async getUserByEmail(email: string, proj: any = {}): Promise<UserDocument> {
        return await this.user_model.findOne({ email: email }, proj).lean().exec() as UserDocument;
    }

    public async saveAccessToken(user_id: string, access_token: string): Promise<void> {
        await this.user_model.updateOne({ _id: new mongoose.Types.ObjectId(user_id) }, { "$set": { access_token: access_token } });
    }
    
    public async clearAccessToken(user_id: string): Promise<void> {
        await this.user_model.updateOne({ _id: new mongoose.Types.ObjectId(user_id) }, { "$set": { access_token: "" } });
    }

}