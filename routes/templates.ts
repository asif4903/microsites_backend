import express, { Request, Response } from 'express';
import { Template } from '../controllers/templates';

const router = express.Router();

router.get('/get_default_template', async (req: Request, res: Response) => {
    try {
        if (!req.query.template_id) {
            return res.status(400).send({
                success: false,
                message: "please provide template_id",
                http_status_code: 400,
                data: null
            });
        }
        const template_obj = new Template();
        const template = await template_obj.getDefaultTemplate(req.query.template_id as string);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: template });
    } catch (error: any) {
        console.error('Error in get_default_template api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

router.get('/get_user_template', async (req: Request, res: Response) => {
    try {
        if (!req.query.template_id) {
            return res.status(400).send({
                success: false,
                message: "please provide template_id",
                http_status_code: 400,
                data: null
            });
        }
        const template_obj = new Template();
        const template = await template_obj.getUserTemplate(req.query.template_id as string, req.user._id as string);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: template });
    } catch (error: any) {
        console.error('Error in get_user_template api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

router.get('/get_default_templates', async (req: Request, res: Response) => {
    try {
        if (!req.query.page) {
            return res.status(400).send({
                success: false,
                message: "please provide page",
                http_status_code: 400,
                data: null
            });
        }
        if (parseInt(req.query.page as string) < 1) {
            return res.status(400).send({
                success: false,
                message: "invalid page",
                http_status_code: 400,
                data: null
            });
        }
        const page: number = parseInt(req.query.page as string);
        const template_obj = new Template();
        const templates = await template_obj.getDefaultTemplates(page);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: templates });
    } catch (error: any) {
        console.error('Error in get_default_templates api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

router.get('/get_user_templates', async (req: Request, res: Response) => {
    try {
        if (!req.query.page) {
            return res.status(400).send({
                success: false,
                message: "please provide page",
                http_status_code: 400,
                data: null
            });
        }
        if (parseInt(req.query.page as string) < 1) {
            return res.status(400).send({
                success: false,
                message: "invalid page",
                http_status_code: 400,
                data: null
            });
        }
        const user_id: string = req.user._id as string;
        const page: number = parseInt(req.query.page as string);
        const template_obj = new Template();
        const templates = await template_obj.getUserTemplates(user_id, page);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: templates });
    } catch (error: any) {
        console.error('Error in get_template api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

router.post('/save_user_template', async (req: Request, res: Response) => {
    try {
        if (!req.body.template_name) {
            return res.status(400).send({
                success: false,
                message: "please provide template_name",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.html) {
            return res.status(400).send({
                success: false,
                message: "please provide html",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.css) {
            return res.status(400).send({
                success: false,
                message: "please provide css",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.js) {
            return res.status(400).send({
                success: false,
                message: "please provide js",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.project_json) {
            return res.status(400).send({
                success: false,
                message: "please provide project_json",
                http_status_code: 400,
                data: null
            });
        }

        const template_obj = new Template();
        const saved_template = await template_obj.saveUserTemplate(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: saved_template });
    } catch (error: any) {
        console.error('Error in get_template api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

router.post('/delete_user_template', async (req: Request, res: Response) => {
    try {
        if (!req.body.template_id) {
            return res.status(400).send({
                success: false,
                message: "please provide template_id",
                http_status_code: 400,
                data: null
            });
        }
        const template_obj = new Template();
        const delete_template_res = await template_obj.deleteUserTemplate(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: delete_template_res });
    } catch (error: any) {
        console.error('Error in get_template api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

export default router;