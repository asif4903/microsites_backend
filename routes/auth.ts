import express, { Request, Response } from 'express';
import { Auth } from '../controllers/auth';
import { AuthMiddleware } from '../middlewares/auth';
const router = express.Router();

router.post('/signup', async (req: Request, res: Response) => {
    try {
        if (!req.body.firstname) {
            return res.status(400).send({
                success: false,
                message: "please provide firstname",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.lastname) {
            return res.status(400).send({
                success: false,
                message: "please provide lastname",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.email) {
            return res.status(400).send({
                success: false,
                message: "please provide email",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.password) {
            return res.status(400).send({
                success: false,
                message: "please provide password",
                http_status_code: 400,
                data: null
            });
        }
        const auth = new Auth();
        const signup_res = await auth.signUp(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: signup_res });
    } catch (error: any) {
        console.error('Error in register api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});
router.post('/login', async (req: Request, res: Response) => {
    try {
        if (!req.body.email) {
            return res.status(400).send({
                success: false,
                message: "please provide email",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.password) {
            return res.status(400).send({
                success: false,
                message: "please provide password",
                http_status_code: 400,
                data: null
            });
        }
        const auth = new Auth();
        const login_res = await auth.login(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: login_res });
    } catch (error: any) {
        console.error('Error in login api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});
router.post('/logout', AuthMiddleware.isAuthenticated, async (req: Request, res: Response) => {
    try {
        const auth = new Auth();
        const login_res = await auth.logout(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: login_res });
    } catch (error: any) {
        console.error('Error in logout api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

export default router;