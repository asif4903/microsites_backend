import express, { Request, Response } from 'express';
import { Form } from '../controllers/forms';

const router = express.Router();

router.post('/save_form_data', async (req: Request, res: Response) => {
    try {
        if (!req.body.user_id) {
            return res.status(400).send({
                success: false,
                message: "please provide user_id",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.form_id) {
            return res.status(400).send({
                success: false,
                message: "please provide form_id",
                http_status_code: 400,
                data: null
            });
        }
        if (!req.body.data) {
            return res.status(400).send({
                success: false,
                message: "please provide data",
                http_status_code: 400,
                data: null
            });
        }

        const form_controller_obj = new Form();
        const saved_form_data = await form_controller_obj.saveFormData(req);
        res.status(200).send({ success: true, message: "Success", http_status_code: 200, data: saved_form_data });
    } catch (error: any) {
        console.error('Error in save_form_data api:', error);
        res.status(500).send({ success: false, message: "Something went wrong!!", http_status_code: 500, data: null, error: error.message });
    }
});

export default router;