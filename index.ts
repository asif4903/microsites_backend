import express, { Express } from "express";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import morgan from "morgan";
import mongoose from "mongoose";
import * as dotenv from "dotenv";
dotenv.config({ path: "./config/.env" });
import { getWriteConnectionToDb } from "./connection";
import { Utils } from "./utils/utils";
import templates_router from "./routes/templates";
import auth_router from "./routes/auth";
import form_router from "./routes/forms";
import { AuthMiddleware } from "./middlewares/auth";

declare global {
    var microsites_write_connection: mongoose.Connection;
}
const app: Express = express();

app.use(express.json({ limit: "5mb" }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(cors());
app.use(helmet());
app.use(morgan("dev"));


async function start() {
    Utils.validateEnvironmentVariables();
    const port = process.env.PORT!;
    global.microsites_write_connection = await getWriteConnectionToDb(process.env.DB_HOST!, process.env.DB_PORT!, "microsites");
    app.use("/api", auth_router);
    app.use("/api", AuthMiddleware.isAuthenticated, templates_router);
    app.use("/api", form_router);
    app.listen(port, () => {
        console.log(`⚡️[server]: Server is running at port: ${port}`);
    });

}

start()
    .then()
    .catch((err) => console.error("Error in starting the process:- ", err));
