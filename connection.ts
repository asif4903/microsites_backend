import mongoose from "mongoose";

export const getReadConnectionToDb = async (host: string, port: string, db: string): Promise<mongoose.Connection> => {
    try {
        const options: mongoose.ConnectOptions = {
            directConnection: true,
            keepAlive: true,
            readPreference: "secondaryPreferred",
        };

        const conn = await mongoose.createConnection(
            `mongodb://${host}:${port}/${db}?readPreference=secondaryPreferred`,
            options
        );
        console.log(`connected to database: ${db} at port: ${port}`);
        // console.log("conn: ", conn)
        return conn;
    } catch (error) {
        console.error("Error connecting to the db:- ", error);
        throw error;
    }
};

export const getWriteConnectionToDb = async (host: string, port: string, db: string): Promise<mongoose.Connection> => {
    try {
        const options: mongoose.ConnectOptions = {
            directConnection: true,
            keepAlive: true,
        };

        const conn = await mongoose.createConnection(
            `mongodb://${host}:${port}/${db}`,
            options
        );
        console.log(`connected to database: ${db} at port: ${port}`);
        // console.log("conn: ", conn)
        return conn;
    } catch (error) {
        console.error("Error connecting to the db:- ", error);
        throw error;
    }
};