import Mongoose = require("mongoose");

export interface IUserTemplate {
    user_id: string;
    template_name: string;
    html: string,
    css: string,
    js: string,
    project_json: any;
    created_at?: Date;
    updated_at?: Date;
};

export const UserTemplateSchema = new Mongoose.Schema({
    user_id: { type: String, required: true },
    template_name: { type: String, required: true },
    html: { type: String },
    css: { type: String },
    js: { type: String },
    project_json: { type: Mongoose.Schema.Types.Mixed },
}, {
    collection: 'user_templates',
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

export interface UserTemplateDocument extends Mongoose.Document, IUserTemplate {
};