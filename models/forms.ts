import Mongoose = require("mongoose");

export interface IForm {
    user_id: string;
    form_id: string;
    data: any
    created_at?: Date;
    updated_at?: Date;
};

export const FormSchema = new Mongoose.Schema({
    user_id: { type: String, required: true },
    form_id: { type: String, required: true },
    data: { type: Mongoose.Schema.Types.Mixed, required: true },
}, {
    collection: 'forms',
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

export interface FormDocument extends Mongoose.Document, IForm {
};