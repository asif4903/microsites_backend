import Mongoose = require("mongoose");

export interface IUser {
    firstname: string;
    lastname: string;
    email: string;
    password?: string,
    access_token?: string,
    created_at?: Date;
    updated_at?: Date;
};

export const UserSchema = new Mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    access_token: { type: String, default: ''},
}, {
    collection: 'users',
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

export interface UserDocument extends Mongoose.Document, IUser {
};