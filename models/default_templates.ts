import Mongoose = require("mongoose");

export interface IDefaultTemplate {
    template_name: string;
    html: string,
    css: string,
    js: string,
    project_json: any;
    created_at?: Date;
    updated_at?: Date;
};

export const DefaultTemplateSchema = new Mongoose.Schema({
    template_name: { type: String, required: true },
    html: { type: String },
    css: { type: String },
    js: { type: String },
    project_json: { type: Mongoose.Schema.Types.Mixed },
}, {
    collection: 'default_templates',
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

export interface DefaultTemplateDocument extends Mongoose.Document, IDefaultTemplate {
};