import { Request } from "express";
import { DefaultTemplateDocument, DefaultTemplateSchema } from "../models/default_templates";
import { IUserTemplate, UserTemplateDocument, UserTemplateSchema } from "../models/user_templates";
import mongoose from "mongoose";

export interface IResponse {
    success: boolean;
    message: string;
    http_status_code: number;
    error?: string;
    data?: any;
}


export class Template {
    private default_templates_model = global.microsites_write_connection?.model("default_templates", DefaultTemplateSchema);
    private user_templates_model = global.microsites_write_connection?.model("user_templates", UserTemplateSchema);
    private static pagination_limit = 15;

    public async getDefaultTemplate(template_id: string): Promise<any> {
        const projection = {
            "_id": 1,
            "template_name": 1,
            "html": 1,
            "css": 1,
            "js": 1,
            "project_json": 1
        };
        const template = await this.getDefaultTemplateById(template_id, projection);
        return template;
    }

    public async getUserTemplate(template_id: string, user_id: string): Promise<any> {
        const projection = {
            "user_id": 1,
            "_id": 1,
            "template_name": 1,
            "html": 1,
            "css": 1,
            "js": 1,
            "project_json": 1
        };
        const template = await this.getUserTemplateById(template_id, user_id, projection);
        return template;
    }

    public async getDefaultTemplates(page: number): Promise<any> {
        const skip = (page - 1) * Template.pagination_limit;
        const projection = {
            "_id": 1,
            "template_name": 1,
            "html": 1,
            "css": 1,
            "js": 1,
            "project_json": 1
        };
        const templates = await this.fetchDefaultTemplates(skip, Template.pagination_limit, projection);
        return templates;

    }

    public async getUserTemplates(user_id: string, page: number): Promise<any> {
        const skip = (page - 1) * Template.pagination_limit;
        const projection = {
            "user_id": 1,
            "_id": 1,
            "template_name": 1,
            "html": 1,
            "css": 1,
            "js": 1,
            "project_json": 1
        };
        const templates = await this.fetchUserTemplates(user_id, skip, Template.pagination_limit, projection);
        return templates;

    }

    public async saveUserTemplate(req: Request): Promise<any> {
        const user_id = req.user._id;
        const template_id = req.body.template_id || "";
        const data = {
            "user_id": user_id,
            "template_name": req.body.template_name,
            "html": req.body.html,
            "css": req.body.css,
            "js": req.body.js,
            "project_json": req.body.project_json
        }
        const saved_result = await this.saveOrUpdateUserTemplate(user_id, data, template_id);
        return saved_result;
    }

    public async deleteUserTemplate(req: Request): Promise<any> {
        const user_id = req.user._id;
        const template_id = req.body.template_id || "";
        const template = await this.getUserTemplateById(template_id, user_id);
        if (!template) {
            throw new Error("User template not found");
        }
        return await this.deleteUserTemplateById(template_id, user_id);
    }

    private async getDefaultTemplateById(template_id: string, projection: any = {}): Promise<DefaultTemplateDocument> {
        const template = await this.default_templates_model.findOne({ "_id": new mongoose.Types.ObjectId(template_id) }, projection).lean().exec() as DefaultTemplateDocument;
        return template;
    }

    private async getUserTemplateById(template_id: string, user_id: string, projection: any = {}): Promise<UserTemplateDocument> {
        const template = await this.user_templates_model.findOne({ "_id": new mongoose.Types.ObjectId(template_id), "user_id": user_id }, projection).lean().exec() as UserTemplateDocument;
        return template;
    }

    private async fetchDefaultTemplates(skip: number, limit: number, projection: any = {}): Promise<DefaultTemplateDocument[]> {
        const templates = await this.default_templates_model.find({}, projection).sort({ "_id": -1 }).skip(skip).limit(limit).lean().exec() as DefaultTemplateDocument[];
        return templates;
    }

    private async fetchUserTemplates(user_id: string, skip: number, limit: number, projection: any = {}): Promise<UserTemplateDocument[]> {
        const templates = await this.user_templates_model.find({ "user_id": user_id }, projection).sort({ "_id": -1 }).skip(skip).limit(limit).lean().exec() as UserTemplateDocument[];
        return templates;
    }
    private async saveOrUpdateUserTemplate(user_id: string, data: IUserTemplate, template_id?: string): Promise<any> {
        if (!template_id) {
            const new_user_template_data = new this.user_templates_model(data);
            return await new_user_template_data.save();
        } else {
            const update_query_filter = {
                "_id": template_id,
                "user_id": user_id
            };
            const update_data = {
                "user_id": user_id,
                "template_name": data.template_name,
                "html": data.html,
                "css": data.css,
                "js": data.js,
                "project_json": data.project_json
            };
            return await this.user_templates_model.findOneAndUpdate(update_query_filter, { $set: update_data }, { new: true });

        }
    }

    private async deleteUserTemplateById(template_id: string, user_id: string): Promise<mongoose.mongo.DeleteResult> {
        const delete_res = await this.user_templates_model.deleteOne({ "_id": new mongoose.Types.ObjectId(template_id), "user_id": user_id });
        return delete_res;
    }

}