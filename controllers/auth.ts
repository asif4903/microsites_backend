import { Request } from "express";
import { User } from "../services/users";
import { PasswordUtils } from "../utils/password_utils";
import { IUser } from "../models/users";
import { JWTUtils } from "../utils/jwt_utils";
export class Auth {
    private user_service = new User();
    public async signUp(req: Request): Promise<any> {
        const firstname = req.body.firstname;
        const lastname = req.body.lastname;
        const email = req.body.email;
        const password = req.body.password;

        const user_exists = await this.checkIfUserAlreadyExists(email);
        if (user_exists) {
            throw new Error("User already exists");
        }
        // hash user password
        const hashed_password = await PasswordUtils.hashPassword(password);

        //create new user with hash password
        const new_user: IUser = {
            "firstname": firstname,
            "lastname": lastname,
            "email": email,
            "password": hashed_password
        };
        let new_created_user = await this.user_service.createUser(new_user);
        new_created_user = JSON.parse(JSON.stringify(new_created_user));
        // generate jwt token and save it in user collection.
        const access_token = JWTUtils.generateAuthToken(new_created_user._id);
        await this.user_service.saveAccessToken(new_created_user._id, access_token);
        new_created_user["access_token"] = access_token;
        delete new_created_user.password;
        return new_created_user;
        // send jwt token in response to login
    }

    public async login(req: Request): Promise<any> {
        const email = req.body.email;
        const password = req.body.password;
        const user = await this.user_service.getUserByEmail(email);
        console.log(user)
        if (!user) {
            throw new Error('Invalid Credentials');
        }
        const is_pass_match = await PasswordUtils.comparePassword(password, user.password as string);
        console.log("is_pass_match: ", is_pass_match)
        if (!is_pass_match) {
            throw new Error('Invalid Credentials');
        }
        const access_token = JWTUtils.generateAuthToken(user._id);
        await this.user_service.saveAccessToken(user._id, access_token);
        delete user.password;
        user['access_token'] = access_token;
        return user;

    }

    public async logout(req: Request): Promise<boolean> {
        const user_id = req.user._id;
        await this.user_service.clearAccessToken(user_id);
        return true;
    }

    private async checkIfUserAlreadyExists(email: string): Promise<boolean> {
        const user_doc = await this.user_service.getUserByEmail(email);
        if (user_doc) {
            return true;
        }
        return false;
    }
}