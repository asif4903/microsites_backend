import { Request } from "express";
import { IForm, FormSchema, FormDocument } from "../models/forms";

export class Form {
    private forms_model = global.microsites_write_connection?.model("forms", FormSchema);

    public async saveFormData(req: Request): Promise<any> {
        const user_id = req.body.user_id;
        const form_id = req.body.form_id;
        const form_data: IForm = {
            "user_id": user_id,
            "form_id": form_id,
            "data": req.body.data,
        }
        const form_save_data = new this.forms_model(form_data);
        return await form_save_data.save();
    }
}